import React from 'react'
import info from "./infoAPI"

function PortfolioShares() {
  return (
    <>
      <div className='ShareContainer'>
        {info.map((elem) => {
          return (
            <div className='shareInner' key={elem.id}>
              <div className='colorLogo'></div>
              <div className='shareText'>
                <p>{elem.percentage}<span> {elem.name}</span></p>
              </div>
            </div>
          )
        })}

      </div>
    </>
  )
}

export default PortfolioShares