import React from 'react'

function Progressbar() {
  return (
    <>
      <div className='progressContainer'>
        <div className='percentage'>
          <h2>70% <span>Stocks</span></h2>
          <h2>28% <span>Bonds</span></h2>
          <h2>2% <span>Stocks</span></h2>
          <h2>70% <span>Stocks</span></h2>
        </div>
        <div className='barWrapper'>
          <div className='barInner'></div>
        </div>
      </div>
    </>
  )
}

export default Progressbar