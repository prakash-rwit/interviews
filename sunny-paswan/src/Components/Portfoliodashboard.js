import React from 'react'
import Pbtn from './Pbtn'
function Portfoliodashboard() {
  return (
    <>
      <div className='portfolioSelection'>
        <div className='p-Type'>
          <h3>Portfolio Type</h3>
          <div className='btnContainer'>
            <Pbtn name="Core" />
            <Pbtn name="All Green" />
            <Pbtn name="Safety Net" />
          </div>
          <p>Just the climate index & green Bonds</p>
        </div>

        <div className='p-Select'>
         <h3>Select Portfolio</h3>
         <label>
        <select>
          <option>Most Bold (80/20)</option>
          <option>Moderately Bold(70/30)</option>
          <option>Moderately Cautious(60/40)</option>
          <option>Cautous(50/50)</option>
          <option>Most Cautous(40/60)</option>
          <option>SafetyNet(15/85)</option>
        </select>

         </label>
        </div>
      </div>
    </>
  )
}

export default Portfoliodashboard