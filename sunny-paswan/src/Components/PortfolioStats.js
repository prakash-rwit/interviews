import React from 'react'

function PortfolioStats() {
  return (
    <>
      <div className='StatsContainer'>
        <div className='statsHeader'>
          <h3>Stats</h3>
          <span>Jan-1,2015-Mar 31,2021</span>
        </div>

        <table>
          <thead>
            <tr>
              <td></td>
              <td>carbon Collective</td>
              <td>INDEX</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Annual return</td>
              <td>12.47%</td>
              <td>12.47</td>
            </tr>
            <tr>
              <td>Risk(STD)</td>
              <td>12.47%</td>
              <td>12.47</td>
            </tr>

            <tr>
              <td>Beta</td>
              <td>12.47%</td>
              <td>12.47</td>
            </tr>

            <tr>
              <td>Alpha</td>
              <td>2.42</td>
              <td>0.10</td>
            </tr>
          </tbody>
        </table>

        <span>Disclosure</span>
      </div>
    </>
  )
}

export default PortfolioStats