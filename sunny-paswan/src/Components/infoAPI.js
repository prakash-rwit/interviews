const info = [
    {
        id:"1",
        name:"Climate Index",
        percentage: "17.8%"
    },
    {
        id:"2",
        name:"Green Bonds",
        percentage: "7.2%"
    },
    {
        id:"3",
        name:"Low Carbon Economy ETFs",
        percentage: "62.2%"
    },
    {
        id:"4",
        name:"Cash",
        percentage: "2%"
    },
    {
        id:"5",
        name:"US Treasury Bonds",
        percentage: "10.8%"
    }
]

export default info