import React from 'react'
import Portfoliodashboard from './Portfoliodashboard'
import PortfolioShares from './PortfolioShares'
import Progressbar from './Progressbar'
import PortfolioStats from './PortfolioStats'
function Portfolio() {
  return (
    <>
      <h1>Portfolio Calculator</h1>
      <div className='portfolio-main'>

        <div className='PortfolioLeft'>
          <Portfoliodashboard />
          <Progressbar />
          <PortfolioShares />
        </div>

        <div className='PortfolioRight'>
          <PortfolioStats />
        </div>
      </div>

    </>
  )
}

export default Portfolio